<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ForumController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.master');
});

// Halaman utama Category
Route::get('/category', [ForumController::class, 'category'])->name('category');
//Halaman untuk ke form
Route::get('/category/create_category', [ForumController::class, 'create_category'])->name('create_category');
//Fungsi untuk post ke database
Route::post('/category', [ForumController::class, 'form_category'])->name('form_category');
//Fungsi untuk get form sesuai dengan id category
Route::get('/category/{category_id}/edit', [ForumController::class, 'edit_category'])->name('edit_category');
//Fungsi untuk edit Category
Route::put('/category/{category_id}', [ForumController::class, 'update_category'])->name('update_category');
//Fungsi untuk Delete Category
Route::delete('/category/{category_id}', [ForumController::class, 'delete_category'])->name('delete_category');

//Fungsi untuk buat pertanyaan
Route::get('/pertanyaan', [ForumController::class, 'pertanyaan'])->name('pertanyaan');
//Fungsi untuk post pertanyaan
Route::post('/pertanyaan', [ForumController::class, 'form_pertanyaan'])->name('form_pertanyaan');

