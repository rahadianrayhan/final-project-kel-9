@extends('layout.master')
@section('title')
    Form Category
@endsection

@section('content')
    <form action="/category" method="POST">
        @csrf
        <div class="form-group">
            <label>Category</label>
            <input type="text" name="category_name" class="form-control">
        </div>
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
