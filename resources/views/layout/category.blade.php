@extends('layout.master')
@section('title')
    Table Category
@endsection

@section('content')
    <table class="table table-hover">
        <div>
            <h3>
                Create Category
            </h3>
            <a href="/category/create_category" class="btn btn-primary" role="button" aria-pressed="true">Create</a>
        </div>
        <br>
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Category List</th>
                <th scope="col">Handle</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($category as $key => $_category)
                <tr class="table-light">
                    <th scope="row">{{ $key + 1 }}</th>
                    <th>{{ $_category->category_name }}</th>
                    <th>
                        <div class="row">
                            <a class="btn btn-primary ml-1 mr-1" href="{{ route('edit_category', $_category->id) }}">
                                edit</a>
                            <form action="/category/{{ $_category->id }}" method="POST">
                                @method('delete')
                                @csrf
                                <button class="btn btn-primary ml-1 mr-1"> delete</button>
                            </form>
                        </div>
                    </th>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
