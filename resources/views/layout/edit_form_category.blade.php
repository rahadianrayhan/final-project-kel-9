@extends('layout.master')
@section('title')
    Form Category
@endsection

@section('content')
    <form action="{{ route('update_category', $category->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Category</label>
            <input type="text" name="category_name" class="form-control" value="{{ $category->category_name }}">
        </div>
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
