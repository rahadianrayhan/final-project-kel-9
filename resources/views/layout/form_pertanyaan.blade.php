@extends('layout.master')
@section('title')
    Form Pertanyaan
@endsection

@section('content')
    <form action="/pertanyaan" method="POST">
        @csrf
        <div class="form-group">
            <label>Pertanyaan</label>
            <input type="text" name="pertanyaan" class="form-control">
        </div>
        @error('pertanyaan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Thumnail</label>
            <input type="text" name="thumnail" class="form-control">
        </div>
        @error('Thumnail')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
