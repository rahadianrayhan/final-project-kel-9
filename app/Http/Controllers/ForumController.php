<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Category;

class ForumController extends Controller
{
    public function category(){
        $category = Category::all();
        return view('layout.category',[
            'category' => $category
        ]);
    }

    public function create_category(){
        return view('layout.form_category');
    }

    public function form_category(Request $_request){
        $_request->validate([
            'category_name' => 'required'
        ]);

        DB::table('category')->insert([
            'category_name' => $_request['category_name']
        ]);

        return redirect('/category');
    }

    public function edit_category($category_id){
        $category = Category::find($category_id);
        return view('layout.edit_form_category', [
            'category' => $category
        ]);
    }

    public function update_category($category_id, Request $_request){
        $_request->validate([
            'category_name' => 'required'
        ]);

        DB::table('category')
        ->where(
            'id', $category_id
        )
        ->update([
            'category_name' => $_request['category_name']
        ]);

        return redirect('/category');
    }

    public function delete_category($category_id){
        DB::table('category')
        ->where(
            'id', $category_id
        )
        ->delete();

        return redirect('/category');
    }

    public function pertanyaan(){
        return view('layout.form_pertanyaan');
    }

    public function form_pertanyaan(){
        $_request->validate([
            'pertanyaan' => 'required',
            'thumnail' => 'required'
        ]);

        DB::table('pertanyaan')->insert([
            'pertanyaan' => $_request['pertanyaan'],
            'thumnail' => $_request['thumnail']
        ]);

        return redirect('/pertanyaan');
    }
}
